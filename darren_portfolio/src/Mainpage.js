import React from "react";
import { NavLink } from "react-router-dom";

function MainPage() {
    return (
    <>
    <header className="full-screen-header">
      <div className="blur-background"></div>
      <h1 className="main-title gradient-text">Darren Bowser</h1>
      <span className="sub-title">
        Hi
      </span>
      <NavLink className="btn header-btn" to="/about">
        Learn More
      </NavLink>
    </header>
  </>
    );
}

export default MainPage
