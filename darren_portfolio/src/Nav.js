import { NavLink } from "react-router-dom";
import { React } from "react";
import "./index.css";

function Nav() {
    return (
        <nav className="nav nav-top">
        <NavLink className="btn" to="/">
            MainPage
        </NavLink>
        </nav>
    )
}

export default Nav
